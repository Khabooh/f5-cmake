# Introduction à CMake

1. CMake
2. Principes de Bases
    + Targets
    + Options
    + Variables

---
Note: Cette série s'incrit dans un usage de CMake orienté pour du C ou C++.

---

## 1. CMake

CMake est outil permettant de contrôler et produire la chaîne de construction d'un programme, en faisant abstraction des spécifités liées aux plate-formes et compilateurs.

CMake ne produit pas directement d'exécutables, mais des fichiers liés à un environnement (détecté ou configuré) qui eux serviront à générer les bibliothèques et fichiers binaires.

Avec un seul fichier de configuration (`CMakeLists.txt`), CMake génère des `Makefile` sous un environnement Unix, des fichiers de projets Visual Studio sous Windows, ou encore des fichiers XCode pour Openium.

## Pratique

Un bon IDE possède normalement de quoi éditer intelligement un fichier `CMakeLists.txt`, mais si vous voulez juste tester et suivre ces notes, Visual Studio Code ou Sublime Text possèdent de bons plugins.

Pour l'installation, un outil GUI existe sous Windows (permettant de visualiser et manipuler les différentes variables de configuration) sur le [site internet](https://cmake.org/), tandis que sous un Unix, vous devriez pouvoir l'installer via votre gestionnaire de paquets préféré (sous Debian par exemple) :

```shell
$ sudo apt install cmake
```

## 2. Principes de Bases

Une des premières choses que l'on peut faire est de spécifier la version minimum requise de CMake, avec la commande `cmake_minimum_required` :

```cmake
# Ceci est un commentaire
# On set ici la version minimum à la version 3.5
cmake_minimum_required (VERSION 3.5.0)
# Cette commande doit être appelée en premier afin de définir les éventuels comportements des fonctions et les politiques CMake
```

Ensuite, on peut venir appeler la commande `project` afin de définir des variables CMake liées au projet, comme son nom, sa version, sa description, une URL etc.

```cmake
project (Our_first_project)
```

Après cette commande sera définie la variable `PROJECT_NAME` (entre autres), qui pourra être utilisée dans le reste du fichier.

---

Le système de build CMake s'organise autour de `targets` logiques, qui correspondent à des exécutables, bibliothèques ou encore `custom target` représentant des `targets` utilitaires, sans fichiers en sortie.

Les dépendances entre `targets` sont déclarées dans le système de build afin de déterminer l'ordre de compilation et les règles de génération des différentes parties.

Par exemple, on considère un code `sample.cpp` qui définit une fonction :

```cpp
#include <iostream>

double eval_lin (double a, double b, double x) {
    return a*x + b;
}

int main () {
    double result = eval_lin (1.5, 3.0, 0.42);
    std::cout << "Evaluated value: " << result << '\n';

    return 0;
}
```

Pour compiler le tout, on va créer une `target` exécutable qui comprendra ce fichier en source !

```cmake
# En gardant le début du fichier évoqué plus haut
add_executable ( ${PROJECT_NAME} sample.cpp )
```

Et tout est bon pour la configuration basique ! On peut lancer la production !

Dans tout son système de génération, CMake génère un certain nombre de fichier (un cache, des fichiers de configuration et de compilation) donc mieux vaut choisir un endroit "adéquat" pour lancer la commande.

Une bonne pratique est la compilation *out-source*, ie les fichiers issus de la compilation ne se mélangent pas avec les fichiers source, et CMake gère très bien ce principe !

On va ici mettre au tout ça ~~au goulag !~~ dans un dossier `build` :

```bash
# ici, "project/" est la racine de votre projet
project/ $ mkdir build && cd build
project/build $ cmake ..
```

Dans le cas le plus simple, la commande `cmake` prend un répertoire racine dans lequel il s'attend à trouver un fichier `CMakeLists.txt`, ici le dossier parent (`..`).

Une fois la commande effectuée, si tout s'est bien passé, vous devriez trouver dans votre dossier de build, entre autres, un `CMakeCache.txt` et un `Makefile` si vous êtes sous un système Unix.

Et là, il ne reste plus qu'à lancer un `make` et admirer le fruit le fruit de notre compilation durement obtenue. (Et il y a même des couleurs si votre terminal le supporte !)

Et si votre compilation a réussi également, vous devriez avoir un bel exécutable tout beau tout propre avec le nom que vous avez indiqué lors de la commande `project` :)

A partir de là, si vous changez le code du fichier `sample.cpp`, vous pouvez juste relancer la commande `make` pour tout refaire, une règle existe dans le `Makefile` qui relancera un `cmake` si nécessaire, et avec les mêmes options que lors du premier appel.
